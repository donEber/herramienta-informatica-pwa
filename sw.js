
const CACHE_STATIC = 'static-v1';
const CACHE_DYNAMIC = 'dynamic-v1';
const CACHE_INMUTABLE = 'inmutable-v1';
const APP_SHELL = [
    // '/',
    'index.html',
    'app.js'
];
const APP_SHELL_INMUTABLE = [
    'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons',
    'https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css',
    'https://cdn.jsdelivr.net/npm/vue/dist/vue.js',
    'https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js'
];
self.addEventListener('install', e => {
    const promCacheStatic = caches.open(CACHE_STATIC)
        .then(cache => cache.addAll(APP_SHELL));
    const promCacheInmutable = caches.open(CACHE_INMUTABLE)
        .then(cache => cache.addAll(APP_SHELL_INMUTABLE));
    e.waitUntil(Promise.all([promCacheStatic, promCacheInmutable]));
})
self.addEventListener('fetch', e => {
    const respuesta = caches.match(e.request)
        .then(res => {
            if (res)
                return res;
            return fetch(e.request)
                .then(res2 => {
                    caches.open(CACHE_DYNAMIC)
                        .then(cache => {
                            cache.put(e.request, res2)
                        });
                    return res2.clone();
                });
        });
    e.respondWith(respuesta)
})
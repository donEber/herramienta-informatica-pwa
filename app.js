if (navigator.serviceWorker)
    navigator.serviceWorker.register('/herramienta-informatica-pwa/sw.js');
    // navigator.serviceWorker.register('sw.js');

var teclado = {
    template:
        `<div>
            Soy un componente sin usar :(
        </div>`,
};
new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data() {
        return {
            numero: '',
            modoDark: true,
            sw: true,
        }
    },
    methods: {
        agregaPunto() {
            if (!this.numero.includes('.'))
                this.numero += '.';
        },
        limpiar() {
            this.numero = ''
        },
        borrar() {
            if (this.numero.length > 0) this.numero = this.numero.substr(0, this.numero.length - 1);
        }
    },
    computed: {
        binario() {
            let aux = this.numero;
            let res = '';
            while (aux != 0) {
                res = (aux & 1 ? 1 : 0) + res;
                aux = aux >> 1;
            }
            return res;
        },
        decimal() {
            if (this.numero == '') return ''
            let res = 0;
            for (let i = 0; i < this.numero.length; i++)
                res += this.numero[i] == 1 ? Math.pow(2, (this.numero.length - i - 1)) : 0;
            return res;
        }
    },
    components: {
        'teclado': teclado
    }
});